import { Provider } from "react-redux";

import "./App.css";
import Header from "./components/header";
import ModalWrapper from "./components/modalwrapper";
import { Users } from "./pages";
import { store } from "./store";

import "antd/dist/antd.css";
import "./App.css";
import { BrowserRouter, Link, Redirect, Route, Switch } from "react-router-dom";
import { Button } from "antd";
import { AppStyle } from "./app.styles";

function AppContent() {
  return (
    <div>
      <ModalWrapper />
      <Header />
      <Switch>
        <Route path="/users" exact>
          <Users />
        </Route>
        <Route path="/profile" exact>
          <div className="dummy-routes">
            <h1>Profile page</h1>
            <Link to="/users">
              <Button type="primary">Go back to users</Button>
            </Link>
          </div>
        </Route>
        <Route path="/settings" exact>
          <div className="dummy-routes">
            <h1>Setings page</h1>
            <Link to="/users">
              <Button type="primary">Go back to users</Button>
            </Link>
          </div>
        </Route>
        <Redirect to="/users" />
      </Switch>
    </div>
  );
}
function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <AppStyle />
        <AppContent />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
