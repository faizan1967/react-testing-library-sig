import { configureStore } from '@reduxjs/toolkit';
import { appreducer } from './reducers';

export const store = configureStore({
  reducer: {
    appreducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
