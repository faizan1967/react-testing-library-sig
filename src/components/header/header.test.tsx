import { waitFor, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { render } from "../../setupTests";
import Header from "./header";
import { headerTestIds } from "./header.testids";

test("Header components render correctly", () => {
  const { getByTestId } = render(<Header />);

  //header title
  expect(getByTestId(headerTestIds.header_title)).toBeInTheDocument();
  expect(getByTestId(headerTestIds.header_title)).toHaveTextContent(
    "React Testing Library SIG"
  );

  //header dropdown
  expect(getByTestId(headerTestIds.header_dropdown)).toBeInTheDocument();
});

test("Header dropdown functionality working correctly", async () => {
  const { getByTestId } = render(<Header />);

  userEvent.click(getByTestId(headerTestIds.header_dropdown));

  await waitFor(
    () =>
      expect(
        screen.getByTestId(headerTestIds.header_dropdown_content)
      ).toBeInTheDocument(),
    { timeout: 6000 }
  );

  const profileElement = getByTestId(
    headerTestIds.header_dropdown_option_profile
  );
  const logoutElement = getByTestId(
    headerTestIds.header_dropdown_option_settings
  );

  expect(
    getByTestId(headerTestIds.header_dropdown_content)
  ).toBeInTheDocument();

  expect(logoutElement).toBeInTheDocument();

  expect(profileElement).toBeInTheDocument();

  expect(profileElement).toHaveTextContent("Profile");

  expect(logoutElement).toHaveTextContent("Settings");

  userEvent.click(profileElement);
  expect(window.location.pathname).toBe("/profile");

  userEvent.click(logoutElement);
  expect(window.location.pathname).toBe("/settings");
});
