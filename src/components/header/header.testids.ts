export const headerTestIds = {
  header_title: "test-header-title",
  header_dropdown: "test-header-more-options-dropdown",
  header_dropdown_content: "test-header-dropdown-content",
  header_dropdown_option_profile:
    "test-header-more-options-dropdown-option-profile",
  header_dropdown_option_settings:
    "test-header-more-options-dropdown-option-settings",
};
