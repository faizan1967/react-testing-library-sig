import { useState } from "react";
import { Dropdown, Button } from "antd";
import {
  CaretDownOutlined,
  CaretUpOutlined,
  UserOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";

import { HeaderStyled, MenuStyled } from "./header.styles";
import { headerTestIds } from "./header.testids";
import { useCallback } from "react";

function Header(): JSX.Element {
  const [dropdownVisibility, setVisibility] = useState<Boolean>(false);

  const onVisibilityChange = useCallback(
    (visible) => {
      // setTimeout(() => {
      setVisibility(visible);
      // }, 1100);
    },
    [dropdownVisibility]
  );

  return (
    <HeaderStyled>
      <div className="left-sec" data-testid={headerTestIds.header_title}>
        React Testing Library SIG
      </div>
      <div className="right-sec">
        <Dropdown
          overlay={
            <MenuStyled data-testid={headerTestIds.header_dropdown_content}>
              <Link
                to="/profile"
                data-testid={headerTestIds.header_dropdown_option_profile}
              >
                <div role="button" onClick={() => onVisibilityChange(false)}>
                  <UserOutlined /> Profile
                </div>
              </Link>
              <div className="divider" />
              <Link
                to="/settings"
                data-testid={headerTestIds.header_dropdown_option_settings}
              >
                <div role="button" onClick={() => onVisibilityChange(false)}>
                  <SettingOutlined /> Settings
                </div>
              </Link>
            </MenuStyled>
          }
          onVisibleChange={onVisibilityChange}
          trigger={["click"]}
          // visible={!!dropdownVisibility}
        >
          <Button
            type="link"
            className="more-options"
            data-testid={headerTestIds.header_dropdown}
          >
            <div>
              <div>Welcome, </div>
              <div>Faizan</div>
            </div>
            <div>
              {dropdownVisibility ? <CaretUpOutlined /> : <CaretDownOutlined />}
            </div>
          </Button>
        </Dropdown>
      </div>
    </HeaderStyled>
  );
}

export default Header;
