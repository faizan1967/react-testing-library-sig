import styled, { css } from "styled-components";

const flexCss = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const HeaderStyled = styled.div`
  height: 45px;
  padding: 0 0px 0px 10px;
  ${flexCss};
  justify-content: space-between;
  color: white;
  background: #2c6aa0;

  .left-sec {
    padding: 15px 12px;
  }

  .right-sec {
    display: flex;
    align-items: center;

    .more-options {
      height: 45px;
      min-width: 90px;
      background: #62a8d1;
      padding: 0 8px;
      color: #ffffff;
      text-align: left;
      display: flex;
      align-items: center;
      font-size: 85%;
      border-left: 1px solid #ddd;

      :hover {
        background: #579ec8;
      }

      div:nth-of-type(1) {
        margin-right: 8px;
      }
    }

    .ant-dropdown {
      top: 44px !important;
    }
  }
`;

export const MenuStyled = styled.div`
  width: 158px;
  box-shadow: 0 2px 4px rgb(0 0 0 / 20%);
  border: 1px solid rgba(0, 0, 0, 0.15);
  background: #fff;
  padding: 5px 0px;
  font-size: 13px;
  color: #333;
  > a:not(.divider) {
    div {
      height: 26px;
      line-height: unset;
      padding: 4px 12px;
      cursor: pointer;
      color: #333;

      :hover {
        background: #fee188;
      }
    }
  }

  .divider {
    height: 1px;
    margin: 9px 0px;
    background: #e5e5e5;
  }
`;
