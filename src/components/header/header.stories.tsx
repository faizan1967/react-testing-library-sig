import Header from "./header";

export default { title: "PageHeader" };

export const HeaderDefault: React.FC = () => {
  return <Header />;
};
