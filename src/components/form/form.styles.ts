import { Form } from "antd";
import styled from "styled-components";

const FormStyled = styled(Form)`
  &.ant-form {
    padding-top: 50px;

    .ant-form-item {
      .ant-form-item-label {
        width: 195px;
        margin-right: 15px;
        .ant-form-item-required {
          font-size: 13px;
          ::after {
            display: none;
          }
          ::before {
            color: red;
          }
        }

        label {
          ::after {
            display: none;
          }
        }
      }

      :last-of-type {
        background-color: #eff3f8;
        border-top: 1px solid #e4e9ee;
        margin-top: 50px;
        padding: 0.3em 1em 0.5em 0.4em;
        width: 100%;
        .ant-form-item-control-input-content {
          display: flex;
          justify-content: flex-end;
          button {
            font-size: 14px;
            margin: 0.5em 0.4em 0.5em 0;
            height: 36px;
          }
        }
      }
    }
  }
`;

export default FormStyled;
