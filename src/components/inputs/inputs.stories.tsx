import { InputStyled } from './inputs.styles';

export default { title: 'InputStyled' };

export const InputDefault: React.FC = () => {
  return <InputStyled />;
};
