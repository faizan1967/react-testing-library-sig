import { SearchInputStyled, InputStyled, TextAreaStyled } from './inputs.styles';

export { SearchInputStyled as SearchInput, InputStyled as Input, TextAreaStyled as TextArea };
