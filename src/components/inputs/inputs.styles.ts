import { Input } from "antd";
import styled from "styled-components";
import { InputProps } from "./inputs.types";

export const SearchInputStyled = styled(Input.Search)`
  .ant-input {
    padding: 6px 10px;
    :hover {
      border-color: ${(props): string => props.theme.borderColors.DARK_GREY};
      box-shadow: none;
    }
    :focus {
      border-color: ${(props): string => props.theme.borderColors.YELLOW_RD};
      box-shadow: none;
    }
  }
  .ant-input-group-addon {
    background: ${(props): string => props.theme.bgColors.BLUE_25};
    button {
      background: transparent;
      border: none;
      .anticon-search {
        color: ${(props): string => props.theme.textColors.WHITE_COLOR};
      }
    }
  }
`;

export const TextAreaStyled = styled(Input.TextArea)<InputProps>`
  .ant-input {
    width: ${(props): string => props?.width || "100%"};
    height: ${(props): string => props?.height || "28px"};
    border-radius: 5px;

    :focus {
      border-color: ${(props): string => props.theme.borderColors.YELLOW_RD};
      box-shadow: none;
    }
    :hover {
      border-color: ${(props): string => props.theme.borderColors.DARK_GREY};
      box-shadow: none;
    }
  }
`;

export const InputStyled = styled(Input)<InputProps>`
  &.ant-input {
    width: ${(props): string => props?.width || "200px"};
    height: ${(props): string => props?.height || "28px"};
    border: 1px solid #d5d5d5;
    border-radius: 5px;

    :not(:disabled) {
      :hover {
        border-color: #b5b5b5;
        box-shadow: none;
      }

      :focus {
        border-color: #f59942;
        box-shadow: none;
      }
    }
  }
`;
