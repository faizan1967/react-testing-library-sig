import styled from "styled-components";
import { InputStyled } from "../../inputs/inputs.styles";

export const InputFieldStyled = styled(InputStyled)<{
  width?: string;
  height?: string;
}>`
  &.ant-input {
    width: ${(props): string => props?.width || "250px"};
    height: ${(props): string => props?.height || "28px"};
    border-radius: 0px;
  }
`;

export const InputFieldContainerStyled = styled.div<{ hasError: boolean }>`
  .error {
    text-align: right;
    color: red;
  }
  .ant-input {
    border-color: ${(props): any => props.hasError && "#e97373 !important"};
    :hover,
    :focus,
    :active {
      border-color: ${(props): any => props.hasError && "#e97373 !important"};
    }
  }
`;
