import { InputFieldStyled } from './inputfield.styles';

export default { title: 'InputField' };

export const InputFieldDefault: React.FC = () => {
  return <InputFieldStyled />;
};
