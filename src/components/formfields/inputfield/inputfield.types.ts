import { InputProps } from 'antd/lib/input';

export type InputFieldType = InputProps & {
  width?: string;
  height?: string;
  error?: string;
  register?: any;
  control: any;
  isPasswordField?: boolean;
};
