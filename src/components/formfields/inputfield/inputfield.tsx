import { Tooltip } from "antd";
import { useController } from "react-hook-form";
import {
  InputFieldContainerStyled,
  InputFieldStyled,
} from "./inputfield.styles";
import { InputFieldType } from "./inputfield.types";

function InputField({
  isPasswordField,
  width,
  name = "",
  control,
  ...props
}: InputFieldType): JSX.Element {
  const {
    field: { ref, ...inputProps },
    fieldState: { error },
  } = useController({
    name,
    control,
  });
  return (
    <Tooltip
      title={
        error?.message && (
          <div data-testid={`test-${name}-input-field-error-message`}>
            {error?.message}
          </div>
        )
      }
      overlayClassName="error-tooltip"
      placement="bottomLeft"
    >
      {isPasswordField ? (
        <InputFieldContainerStyled hasError={!!error?.message}>
          <InputFieldStyled
            {...props}
            {...inputProps}
            ref={ref}
            type="password"
            data-testid={`test-${name}-password-field`}
          />
        </InputFieldContainerStyled>
      ) : (
        <InputFieldContainerStyled hasError={!!error?.message}>
          <InputFieldStyled
            {...props}
            {...inputProps}
            ref={ref}
            data-testid={`test-${name}-input-field`}
            autoComplete="off"
          />
        </InputFieldContainerStyled>
      )}
    </Tooltip>
  );
}

export default InputField;
