import { Modal } from 'antd';
import styled from 'styled-components';

export const ModalStyled = styled(Modal)<{ width: string}>`
  .ant-modal-content {
    .ant-modal-close {
      .ant-modal-close-x {
        line-height: 74px;
        width: 62px;

        svg {
          path {
            fill: #d15b47;
          }

          :hover {
            path {
              opacity: 0.5;
            }
          }
        }
      }
    }

    .ant-modal-header {
      padding: 12px 10px 10px 26px;
      background: #f1f1f1;
      height: 61px;
      display: flex;
      align-items: center;

      .ant-modal-title {
        color: #669fc7;
        font-size: 25px;
      }
    }

    .ant-modal-body {
      padding: 0px;
    }

    .ant-modal-footer {
      display: none;
    }
  }
`;
