import { useCallback } from "react";

import { useDispatch, useSelector } from "react-redux";
import { CreateUserForm } from "../../pages/users/features";
import { setModal } from "../../reducers";
import { AppState } from "../../reducers/appreducer/appreducer.types";
import { RootState } from "../../store";
import { crossIcon } from "../../svgs";
import { ModalStyled } from "./modalwrapper.styles";
import { getModalWidth } from "./modalwrapper.utils";

function getModalContent(id: AppState["modal"]["modalId"]): JSX.Element {
  switch (id) {
    case "create_user":
      return <CreateUserForm />;
    default:
      return <div />;
  }
}

function ModalWrapper(): JSX.Element {
  const { modal: { modalTitle, visible, modalId = "create_user" } = {} } =
    useSelector((state: RootState) => state.appreducer);
  const dispatch = useDispatch();

  const onClose = useCallback(() => {
    dispatch(setModal({}));
  }, [visible]);

  return (
    <ModalStyled
      title={modalTitle}
      visible={visible}
      onCancel={onClose}
      width={getModalWidth(modalId)}
      destroyOnClose
      closeIcon={crossIcon}
      centered
    >
      {getModalContent(modalId)}
    </ModalStyled>
  );
}

export default ModalWrapper;
