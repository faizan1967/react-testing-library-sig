import { AppState } from "../../reducers/appreducer/appreducer.types";

export function getModalWidth(id: AppState["modal"]["modalId"]): string {
  switch (id) {
    case "create_user":
      return "600px";
    default:
      return "500px";
  }
}
