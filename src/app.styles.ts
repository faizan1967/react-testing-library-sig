import { createGlobalStyle } from "styled-components";

export const AppStyle = createGlobalStyle`
html{
  body{
    font-family: RobotoRegular;
  }
}
  .error-tooltip{
        max-width: 228px;
        .ant-tooltip-content {
          background: #e97373;
          min-width: 150px;
          border: none;
          white-space: normal;
          .ant-tooltip-arrow {
            .ant-tooltip-arrow-content {
              background: #e97373;
              border: 1px solid #e97373;
            }
          }
          .ant-tooltip-inner {
            padding: 9px 11px;
            background: #e97373;
            color: white;
            max-width: 260px;
            font-size: 13px;
            font-family: RobotoRegular;
          }
        }
      }

`;
