import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppState } from './appreducer.types';

const initialState: AppState = {
  modal: {},
};

export const appSlice = createSlice({
  name: 'appreducer',
  initialState,
  reducers: {
    setModal: (state, action: PayloadAction<AppState['modal']>) => ({
      ...state,
      modal: action.payload,
    }),
  },
});

export const { setModal } = appSlice.actions;

export default appSlice.reducer;
