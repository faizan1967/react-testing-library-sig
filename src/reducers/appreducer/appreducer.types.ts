export interface AppState {
  modal: {
    modalTitle?: string;
    visible?: boolean;
    modalId?: 'create_user';
    modalProps?: { [key: string]: any };
  };
}
