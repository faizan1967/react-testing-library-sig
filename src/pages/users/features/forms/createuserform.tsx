import { Button } from "antd";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import FormStyled from "../../../../components/form";
import { InputField } from "../../../../components/formfields";
import { createUserSchema } from "../../../../utils/validationschemas/createuserschema";
import { usersTestIds } from "../../users.testids";
import { useDispatch } from "react-redux";
import { useCallback } from "react";
import { setModal } from "../../../../reducers";

type FormValue = {
  username: string;
  email: string;
  password: string;
  address: string;
};

const { Item } = FormStyled;

function CreateUserForm() {
  const {
    control,
    formState: { isValid },
  } = useForm<FormValue>({
    defaultValues: { address: "", password: "", email: "", username: "" },
    resolver: yupResolver(createUserSchema),
    mode: "onChange",
  });
  const dispatch = useDispatch();

  const onClose = useCallback(() => {
    dispatch(setModal({}));
  }, []);

  return (
    <FormStyled data-testid={usersTestIds.add_user_form}>
      <Item label="Username" required>
        <InputField name="username" control={control} />
      </Item>
      <Item label="Email" required>
        <InputField name="email" control={control} />
      </Item>
      <Item label="Address" required>
        <InputField name="address" control={control} />
      </Item>
      <Item label="Password" required>
        <InputField name="password" control={control} isPasswordField />
      </Item>
      <Item>
        <Button
          type="primary"
          data-testid={usersTestIds.add_user_cancel_btn}
          onClick={onClose}
        >
          Cancel
        </Button>
        <Button
          type="primary"
          data-testid={usersTestIds.add_user_submit_btn}
          disabled={!isValid}
        >
          Add
        </Button>
      </Item>
    </FormStyled>
  );
}

export { CreateUserForm };
