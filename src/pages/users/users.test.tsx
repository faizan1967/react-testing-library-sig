import { screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";
import { render } from "../../setupTests";
import {
  INVALID_EMAIL,
  INVALID_USER_NAME,
  MAX_LENGTH_USER_NAME,
  REQUIRED_FIELD,
} from "../../utils/validationschemas/createuserschema";
import Users from "./users";
import { usersTestIds } from "./users.testids";

test("users component render correctly", () => {
  const { getByTestId } = render(<Users />);

  expect(getByTestId(usersTestIds.users_title)).toBeInTheDocument();
  expect(getByTestId(usersTestIds.add_user_btn)).toBeInTheDocument();

  expect(getByTestId(usersTestIds.users_title)).toHaveTextContent(
    "React Testing Library SIG"
  );
  expect(getByTestId(usersTestIds.add_user_btn)).toHaveTextContent("Add User");
});

test("Add user action working correctly", async () => {
  const { getByTestId } = render(<Users />);

  const btnElement = getByTestId(usersTestIds.add_user_btn);
  userEvent.click(btnElement);

  expect(screen.getByTestId(usersTestIds.add_user_form)).toBeInTheDocument();
});

test("Add user form rendered correctly", async () => {
  const { getByTestId } = render(<Users />);

  const btnElement = getByTestId(usersTestIds.add_user_btn);
  userEvent.click(btnElement);

  expect(screen.getByTestId(usersTestIds.username_field)).toBeInTheDocument();
  expect(screen.getByTestId(usersTestIds.email_field)).toBeInTheDocument();
  expect(screen.getByTestId(usersTestIds.address_field)).toBeInTheDocument();
  expect(screen.getByTestId(usersTestIds.password_field)).toBeInTheDocument();
  expect(
    screen.getByTestId(usersTestIds.add_user_submit_btn)
  ).toBeInTheDocument();
  expect(
    screen.getByTestId(usersTestIds.add_user_cancel_btn)
  ).toBeInTheDocument();

  //action btns text render correctly

  expect(
    screen.getByTestId(usersTestIds.add_user_submit_btn)
  ).toHaveTextContent("Add");
  expect(
    screen.getByTestId(usersTestIds.add_user_cancel_btn)
  ).toHaveTextContent("Cancel");
});

test("Add user form fields working correctly", async () => {
  const { getByTestId } = render(<Users />);
  const promise = Promise.resolve();

  const btnElement = getByTestId(usersTestIds.add_user_btn);
  const usernameFieldElement = getByTestId(usersTestIds.username_field);
  const emailFieldElement = getByTestId(usersTestIds.email_field);
  const addressFieldElement = getByTestId(usersTestIds.address_field);
  userEvent.click(btnElement);

  userEvent.type(usernameFieldElement, "hy");
  expect(usernameFieldElement).toHaveValue("hy");

  userEvent.type(emailFieldElement, "dummy@gmail.com");
  expect(emailFieldElement).toHaveValue("dummy@gmail.com");

  userEvent.type(addressFieldElement, "bahawalpur");
  expect(addressFieldElement).toHaveValue("bahawalpur");

  await act(() => promise);
});

test("Add user form fields validations working correctly", async () => {
  const { getByTestId } = render(<Users />);
  const promise = Promise.resolve();

  const btnElement = getByTestId(usersTestIds.add_user_btn);
  const usernameFieldElement = getByTestId(usersTestIds.username_field);
  const emailFieldElement = getByTestId(usersTestIds.email_field);
  const addressFieldElement = getByTestId(usersTestIds.address_field);
  userEvent.click(btnElement);

  //invalid username field validation
  userEvent.type(usernameFieldElement, "hy@");
  userEvent.hover(usernameFieldElement);
  await waitFor(() =>
    screen.findByTestId(usersTestIds.username_field_error_msg)
  );

  expect(
    screen.getByTestId(usersTestIds.username_field_error_msg)
  ).toHaveTextContent(INVALID_USER_NAME);

  //username field reached max character validation
  userEvent.clear(usernameFieldElement);
  userEvent.type(usernameFieldElement, "Albertjohnbravo1234");
  userEvent.hover(usernameFieldElement);
  await waitFor(() =>
    screen.findByTestId(usersTestIds.username_field_error_msg)
  );

  expect(
    screen.getByTestId(usersTestIds.username_field_error_msg)
  ).toHaveTextContent(MAX_LENGTH_USER_NAME);

  //email validation
  userEvent.type(emailFieldElement, "dummy");
  userEvent.click(usernameFieldElement);
  userEvent.hover(emailFieldElement);

  await waitFor(() => screen.findByTestId(usersTestIds.email_field_error_msg));

  expect(screen.getByTestId(usersTestIds.email_field_error_msg)).toHaveTextContent(
    INVALID_EMAIL
  );

  //address validation
  userEvent.type(addressFieldElement, "dummy");
  userEvent.clear(addressFieldElement);
  userEvent.hover(addressFieldElement);
  await waitFor(() =>
    screen.findByTestId(usersTestIds.address_field_error_msg)
  );

  expect(
    screen.getByTestId(usersTestIds.address_field_error_msg)
  ).toHaveTextContent(REQUIRED_FIELD);

  await act(() => promise);
});

test("Submit button should be disabled unless all fields have a valid value", async () => {
  const { getByTestId ,unmount} = render(<Users />);
  const promise = Promise.resolve();

  const btnElement = getByTestId(usersTestIds.add_user_btn);
  const usernameFieldElement = getByTestId(usersTestIds.username_field);
  const emailFieldElement = getByTestId(usersTestIds.email_field);
  userEvent.click(btnElement);

  userEvent.type(usernameFieldElement, "faiz@n");
  userEvent.type(emailFieldElement, "invalidemail");

  expect(screen.getByTestId(usersTestIds.add_user_submit_btn)).toBeDisabled();

  await act(() => promise);
  unmount();
});

test("Submit button should be enable when all fields have a valid value", async () => {
  const { getByTestId } = render(<Users />);
  const promise = Promise.resolve();

  const btnElement = getByTestId(usersTestIds.add_user_btn);
  const usernameFieldElement = getByTestId(usersTestIds.username_field);
  const emailFieldElement = getByTestId(usersTestIds.email_field);
  const passwordElement =getByTestId(usersTestIds.password_field);
  const addressElement =getByTestId(usersTestIds.address_field);

  userEvent.click(btnElement);

  userEvent.type(usernameFieldElement, "faizn");
  userEvent.type(emailFieldElement, "validemail@gmail.com");
  userEvent.type(passwordElement, "faizn"); 
  userEvent.type(addressElement, "faizn");

  await waitFor(()=>expect(screen.getByTestId(usersTestIds.add_user_submit_btn)).not.toBeDisabled())
  expect(screen.getByTestId(usersTestIds.add_user_submit_btn)).not.toBeDisabled();

  await act(() => promise);
});
