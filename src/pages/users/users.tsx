import { Button } from "antd";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { setModal } from "../../reducers";
import { UsersStyled } from "./users.styles";
import { usersTestIds } from "./users.testids";

function Users() {
  const dispatch = useDispatch();

  const onAddUserClick = useCallback(() => {
    dispatch(
      setModal({
        modalId: "create_user",
        modalTitle: "Add User",
        visible: true,
      })
    );
  }, []);

  return (
    <UsersStyled>
      <div className="title" data-testid={usersTestIds.users_title}>
        React Testing Library SIG
      </div>
      <Button
        type="primary"
        onClick={onAddUserClick}
        data-testid={usersTestIds.add_user_btn}
      >
        Add User
      </Button>
    </UsersStyled>
  );
}

export default Users;
