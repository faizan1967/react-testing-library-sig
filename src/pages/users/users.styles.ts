import styled from "styled-components";

export const UsersStyled = styled.div`
  height: calc(100vh - 50px);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  .title {
    font-size: 26px;
    margin-bottom: 10px;
  }
`;
