import * as yup from "yup";

export const REQUIRED_FIELD = "This is a required field";
export const INVALID_EMAIL = "Invalid email";
export const INVALID_USER_NAME =
  "Username cannot contain special letters other than hyphen and underscore";
export const MAX_LENGTH_USER_NAME =
  "Field exceeds the maximum limit of 16 characters";

export const USER_NAME_VALIDATOR = new RegExp(/^[a-zA-Z0-9_ -]{0,177}$/);

export const createUserSchema = yup.object().shape({
  username: yup
    .string()
    .required(REQUIRED_FIELD)
    .matches(USER_NAME_VALIDATOR, INVALID_USER_NAME)
    .max(16, MAX_LENGTH_USER_NAME),
  email: yup.string().email(INVALID_EMAIL).required(REQUIRED_FIELD),
  password: yup.string().required(REQUIRED_FIELD),
  address: yup.string().required(REQUIRED_FIELD),
});
